function zitemErnskraut:OnNimm()
    zitemErnskraut:MoveTo(Player)
    gotkraut = true
    zitemErnskraut.Commands.Nimm.Enabled = false
end
function zitemCompletionCode:OnZeige()
    Wherigo.MessageBox({
        Text = "Dein Completion Code lautet \"" .. Player.CompletionCode .. "\"."
    })
end
function zitemErnskraut:OnGib()
    if timerrunning == true then
        Wherigo.MessageBox({
            Text = "\"Ah, da ist ja das Ernskraut! Und so schnell! Vielen Dank! Und hier, hier hast du meine Schaufel! Pass aber drauf auf, und bring sie mir heil wieder zurueck!\""
        })
    else
        Wherigo.MessageBox({
            Text = "\"Na, kommst du doch noch mit dem Ernskraut! Aber die 5 Minuten sind eigentlich schon um. Naja, ich bin ja nicht so, hier hast du meine Schaufel! Pass aber drauf auf, und bring sie mir heil wieder zurueck!\""
        })
    end
    ztaskBesorgeTheodasErnskraut.Complete = true
    ztaskBesorgdireineSchaufel.Complete = true
    if autosave == true then
        cartDasGeheimnisderverschwundenenVilla:RequestSync()
    end
    zitemSchaufel:MoveTo(Player)
    zitemErnskraut:MoveTo(nil)
end

function zitemGrab:OnGraben()
    Wherigo.GetInput(zinputcodeGrab)
end

