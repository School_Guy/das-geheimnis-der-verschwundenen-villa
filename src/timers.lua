function ztimerkraeutertimer:OnStart()
    timerrunning = true
end

function ztimerkraeutertimer:OnTick()
    timerrunning = false
    if ztaskBesorgeTheodasErnskraut.Complete == false then
        Wherigo.MessageBox({
            Text = "Oje! Die 5 Minuten sind vorbei, und Theo hat noch nicht das Ernskraut, Nun ja, Theo scheint ja ein warmherziger Mensch zu sein - vielleicht kannst du ihn ja doch noch ueberreden..."
        })
    end
    ztimerkraeutertimer:Stop()
end