function zcharacterBurgherrvonWeintraub:OnRede()
    if talkedBurgherr == true then
        Wherigo.MessageBox({
            Text = "\"Was will er wieder hier? Hat er seinen Mut gefunden? Ist er etwa nun bereit, diesen Auftrag anzunehmen?\"",
            Buttons = {"Gewiss", "Mitnichten"},
            Callback = cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB1
        })
    else
        talkedBurgherr = true
        Wherigo.MessageBox({
            Text = "\"Endlich kommet er! Haben wir ihm nicht ausrichten lassen, dass es sich um eine Angelegenheit hoechster Dringlichkeit handelt? Da hat er uns aber lange warten lassen! Etwas Schreckliches ist geschehen: ein GESPENST treibt sein Unwesen auf unserer Burg. Und das wo wir doch heute abend zum alljaehrlichen Burgball empfangen. Wir haben ihn rufen lassen, damit er uns dieses Gespenstes entledigt. Ist er gewillt, diesen Auftrag anzunehmen?\"",
            Buttons = {"Gewiss", "Mitnichten"},
            Callback = cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB1
        })
    end
end

function zcharacterTheo:OnRede()
    flag = false
    if ztaskSprichmitTheo.Active == true and ztaskSprichmitTheo.Complete == false then
        ztaskSprichmitTheo.Complete = true
        if autosave == true then
            cartDasGeheimnisderverschwundenenVilla:RequestSync()
        end
        zoneVerschwundeneVilla.Visible = true
        Wherigo.MessageBox({
            Text = "\"Ah, du kommst wegen dem Geist - ja, der hat mir einen ordentlichen Schrecken eingejagt, das hat er! Ich hab ja grad so viel Arbeit - der Herr will ja seinen Garten picobello wenn heute abend seine Gaeste zum Ball kommen. Also hab ich gestern bis in die Nacht gearbeitet. Und da hab ich ihn gesehen! Mir ist das Herz in die Hose gerutscht, ja das isses! Es ist aus der Burg gekommen, quer durch den Garten, dann durch den Ausgang und schliesslich Richtung Verschwundene Villa geschwebt. Da, ich zeig dir wo das ist! Viel Glueck, ich muss jetzt weiterarbeiten!\"",
            Callback = cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB3
        })
        flag = true
    end
    if ztaskBesorgdireineSchaufel.Active == true and ztaskBesorgdireineSchaufel.Complete == false and ztaskBesorgeTheodasErnskraut.Active == false then
        ztaskBesorgeTheodasErnskraut.Active = true
        zoneKraeuterwiese.Visible = true
        zitemErnskraut.Commands.Nimm.Enabled = true
        Wherigo.MessageBox({
            Text = "Du bittest Theo um die Schaufel. \"Sag mal, Freund, wie stellst du dir das vor? Ich arbeite hier wie ein Hund, komme sowieso schon nicht nach, und jetzt willst du mir mein Arbeitsgeraet wegnehmen?!? Grad hat mir der Herr ausrichten lassen, dass er auch noch Ernskraut benoetigt. Wie soll ich das denn schaffen? Oder willst du mir vielleicht helfen? Gut, ich mach dir einen Vorschlag: Besorg mir das Kraut von der versteckten Wiese da hinten, und sei in spaetestens 5 Minuten wieder da. Wenn du das schaffst, borg ich dir meine Schaufel!\". Du willigst ein, und Theo erklaert dir wo du die Wiese findest.",
            Callback = cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB4
        })
        flag = true
    end
    if ztaskBesorgeTheodasErnskraut.Active == true and ztaskBesorgeTheodasErnskraut.Complete == false and gotkraut == false and flag == false then
        Wherigo.MessageBox({
            Text = "Und? Wo bleibt das Kraut? Wir haben eine Abmachung!!"
        })
        flag = true
    end
    if ztaskBesorgeTheodasErnskraut.Active == true and ztaskBesorgeTheodasErnskraut.Complete == false and gotkraut == true and timerrunning == true then
        Wherigo.MessageBox({
            Text = "\"Ah, da ist ja das Ernskraut! Und so schnell! Vielen Dank! Und hier, hier hast du meine Schaufel! Pass aber drauf auf, und bring sie mir heil wieder zurueck!\""
        })
        ztaskBesorgeTheodasErnskraut.Complete = true
        ztaskBesorgdireineSchaufel.Complete = true
        if autosave == true then
            cartDasGeheimnisderverschwundenenVilla:RequestSync()
        end
        zitemSchaufel:MoveTo(Player)
        zitemErnskraut:MoveTo(nil)
        zitemErnskraut.Commands.Gib.Enabled = false
        flag = true
    end
    if ztaskBesorgeTheodasErnskraut.Active == true and ztaskBesorgeTheodasErnskraut.Complete == false and gotkraut == true and timerrunning == false then
        Wherigo.MessageBox({
            Text = "\"Na, kommst du doch noch mit dem Ernskraut! Aber die 5 Minuten sind eigentlich schon um. Naja, ich bin ja nicht so, hier hast du meine Schaufel! Pass aber drauf auf, und bring sie mir heil wieder zurueck!\""
        })
        ztaskBesorgeTheodasErnskraut.Complete = true
        ztaskBesorgdireineSchaufel.Complete = true
        if autosave == true then
            cartDasGeheimnisderverschwundenenVilla:RequestSync()
        end
        zitemSchaufel:MoveTo(Player)
        zitemErnskraut:MoveTo(nil)
        zitemErnskraut.Commands.Gib.Enabled = false
        flag = true
    end
    if cartDasGeheimnisderverschwundenenVilla.Complete == true then
        Wherigo.MessageBox({
            Text = "\"Na sowas, ich habs schon gehoert - du hast uns vom Geist befreit! Jetzt kann der Herr endlich wieder gut schlafen, und vor allem seinen Ball geniessen! Und meine Schaufel bringst du mir auch zurueck! Zum Dank schenk ich dir dieses Easteregg!\"",
            Callback = cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB9
        })
        flag = true
    end
    if flag == false then
        Wherigo.MessageBox({
            Text = "\"Lieber Freund, du kannst dir vorstellen, ich wuerd jetzt wirklich auch gern tratschen - aber keine Zeit, der Herr will fuer seinen Ball heute Abend einen schoenen Garten, und es ist noch so viel zu tun. Also bitte, halt mich nicht laenger vom Arbeiten ab!\""
        })
    end
end

function zcharacterIgnatiaFeuerschwanz:OnRede()
    talkedIgnatia = true
    flag = false
    if ztaskFindeFrederiksGebeine.Active == false then
        Wherigo.MessageBox({
            Text = "Ja sowas! Hi, hi! Da sucht ja noch einer nach dem armen Frederik. Keine Angst vor dem Fluch, mmh? Hi, hi!",
            Buttons = {"Fluch?"},
            Callback = cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB5
        })
        flag = true
    end
    if ztaskFindeFrederiksGebeine.Active == true and ztaskBesorgdireineSchaufel.Active == false and flag == false then
        Wherigo.GetInput(zinputcodeGebeine)
        flag = true
    end
    if ztaskBesorgdireineSchaufel.Active == true and ztaskBesorgdireineSchaufel.Complete == false then
        Wherigo.MessageBox({
            Text = "Und? Deine Schaufel ist wohl unsichtbar! Hi, hi!",
            Buttons = {"Ok", "Hint"},
            Callback = cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB7
        })
        flag = true
    end
    if ztaskBesorgdireineSchaufel.Active == true and ztaskBesorgdireineSchaufel.Complete == true and ztaskFindedasGrabvonFrederiksFrau.Active == false then
        Wherigo.MessageBox({
            Text = [[
Da kommt ja der Totengraeber! Hi, hi! Samt Schaufel!<BR>
<BR>
Ok, also der Lieblingsplatz von Frederik und Herliande war dort oben, wo man so einen schoenen Ausblick hat! Diese Turteltauben! Hi, hi! Also, auf gehts, bereite dem Fluch ein Ende!]],
            Callback = cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB2
        })
        zoneLuisenruhe.Visible = true
        ztaskFindedasGrabvonFrederiksFrau.Active = true
        flag = true
    end
    if cartDasGeheimnisderverschwundenenVilla.Complete == true then
        Wherigo.MessageBox({
            Text = "Du hast es geschafft! Hi, hi! Gratuliere!"
        })
        flag = true
    end
    if flag == false then
        Wherigo.MessageBox({
            Text = "Ja, ja, is schon witzig. Hi, hi!"
        })
    end
end
