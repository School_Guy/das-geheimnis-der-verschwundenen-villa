function zinputcodeGebeine:OnGetInput(input)
    codeGebeine = input
    IM32K = string.upper(codeGebeine)
    if Wherigo.NoCaseEquals(IM32K, "N85WB") then
    end
    if Wherigo.NoCaseEquals(IM32K, "E88KH") then
    end
    if Wherigo.NoCaseEquals(IM32K, "G52AQ") then
        IM32K = "U33BD"
        zitemGebeine:MoveTo(Player)
        ztaskFindeFrederiksGebeine.Complete = true
        if autosave == true then
            cartDasGeheimnisderverschwundenenVilla:RequestSync()
        end
        Wherigo.MessageBox({
            Text = [[
&lt;die Gebeine sind nun in deinem Inventar, bitte vergiss nicht die realen "Gebeine" wieder gut am selben Ort zu verstecken!&gt;<BR>
<BR>
]],
            Callback = cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB6
        })
        ztaskBesorgdireineSchaufel.Active = true
    else
        Wherigo.MessageBox({
            Text = "&lt;der Code ist leider FALSCH! Hast du auch wirklich die Gebeine gefunden?!? Lies noch mal genau nach, und probiers nochmal!&gt;"
        })
    end
    if Wherigo.NoCaseEquals(IM32K, "O53OP") then
    end
end

function zinputcodeGrab:OnGetInput(input)
    codeGrab = input
    F87KJ = string.upper(codeGrab)
    if Wherigo.NoCaseEquals(IM32K, "W61KY") then
    end
    if Wherigo.NoCaseEquals(F87KJ, "MJ73W") then
        IM32K = "F60QA"
        IM32K = "Q99MH"
        zitemCompletionCode:MoveTo(Player)
        ztaskFindedasGrabvonFrederiksFrau.Complete = true
        if autosave == true then
            cartDasGeheimnisderverschwundenenVilla:RequestSync()
        end
        zitemGrab.Commands.Graben.Enabled = false
        zitemGebeine:MoveTo(nil)
        zitemGrab.Description = [[
Das Grab, in dem Frederik und seine Frau Herliande nun im ewigen Frieden vereint sind<BR>
&lt;Gratuliere, du hast diesen Wherigo-Cache geschafft! Schau in dein Inventar nach dem Completion Code!&gt;]]
        Wherigo.MessageBox({
            Text = [[
Du schaufelst an der Stelle des Grabes bis das Loch gross genug ist, um Frederiks Gebeine aufzunehmen. Vorsichtig legst du sie hinein... ploetzlich faehrt ein gruenlich-leuchtendes Etwas aus den Knochen! Waehrend es schnell blasser wird, bildest du dir ein eine leise Stimme zu hoeren: "Danke, Ihr habt mich erloest...". Die Stimme verstummt, das gruene Leuchten verschwindet - du hast es geschafft, du hast den Fluch gebrochen!<BR>
&lt;Du findest deinen Completion Code und weitere Hinweise in deinem Inventar&gt;<BR>
]]
        })
        cartDasGeheimnisderverschwundenenVilla.Complete = true
        if autosave == true then
            cartDasGeheimnisderverschwundenenVilla:RequestSync()
        end
    else
        Wherigo.MessageBox({
            Text = "&lt;der Code ist leider FALSCH! Du findest den Code am Deckel der Dose und am Logbuch.&gt;",
            Buttons = {"Ok", "Hint"},
            Callback = cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB8
        })
    end
end