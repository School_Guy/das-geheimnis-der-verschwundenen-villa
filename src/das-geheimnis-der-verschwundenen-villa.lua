IM32K = ""
visitluisenruhe = false
flag = false
codeGrab = ""
codeGebeine = ""
gotkraut = false
talkedIgnatia = false
talkedBurgherr = false
F87KJ = ""
autosave = true
timerrunning = false

function cartDasGeheimnisderverschwundenenVilla:OnStart()
    Wherigo.MessageBox({
        Text = [[
Herzlich Willkommen zu diesem kleinen &nbsp;Abenteuer!<BR>
<BR>
Der Burgherr hat nach dir rufen lassen, es geht um eine Angelegenheit hoechster Dringlichkeit. Er erwartet dich vor seiner Burg - eile sofort hin, er wartet nicht gerne!<BR>
<BR>
&lt;Auto-Save aktiv; speichert automatisch den Spielstand sobald ein Task erfuellt wurde&gt;]],
        Media = zmediaTitelbild,
        Buttons = {"Weiter"}
    })
end

function cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB7(action)
    if ztaskSprichmitTheo.Complete == true and action == "Button2" then
        Wherigo.MessageBox({
            Text = "&lt;Du hast schon eine Schaufel auf deinem Weg hierher gesehen - ueberleg einmal wer sie hat...&gt;"
        })
    end
    if ztaskSprichmitTheo.Complete == false and action == "Button2" then
        Wherigo.MessageBox({
            Text = "&lt;Denk an den anderen Task, den du noch nicht erledigt hast!&gt;"
        })
    end
end
function cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB8(action)
    if action == "Button2" then
        Wherigo.MessageBox({
            Text = "&lt;Such eine Bank mit besonderer Aussicht und einem Felsen dahinter. Schaut der Felsen nicht verdaechtig aus?&gt;"
        })
    end
end
function cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB6(action)
    if action ~= nil then
        Wherigo.Dialog({
            {
                Text = "Hi, hi! Ich glaubs nicht! Bin ich so blind, oder was? Da suchen seit Generationen hunderte von Gespensterforschern wie mir nach den Gebeinen, und du hast sie im Handumdrehen gefunden! Man koennte fast meinen, du hast Uebung im Finden versteckter Dinge! Hi, hi!"
            },
            {
                Text = "Also, dann brechen wir jetzt ein fuer alle mal den Fluch des armen Frederik! Ich weiss zwar nicht genau wo das Grab von Herliande ist, aber ungefaehr kann ich's dir sagen..."
            },
            {
                Text = "Aber halt! Hi, hi! Es hat ja gar keinen Sinn, wenn ich dich da jetzt hinschicke! Dir fehlt ja ein wichtiges Utensil. Na, kommst selber drauf was? Hi, hi!"
            },
            {
                Text = [[
Na eine Schaufel natuerlich! Wie willst denn sonst die Gebeine vergraben? Hi, hi!<BR>
<BR>
Also, besorg dir eine Schaufel, dann komm wieder, und ich sag dir wo du nach dem Grab suchen kannst!]]
            }
        })
    end
end
function cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB9(action)
    if action ~= nil then
        zitemSchaufel:MoveTo(zcharacterTheo)
        zitemEasterEgg:MoveTo(Player)
        Wherigo.ShowScreen(Wherigo.DETAILSCREEN, zitemEasterEgg)
    end
end
function cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB5(action)
    if action ~= nil then
        Wherigo.Dialog({
            {
                Text = [[
Du bist hier auf Gespensterjagd und kennst gar nicht die Hintergruende? Das ist ja echt witzig! Hi, hi!<BR>
<BR>
Also es ist so: Einst stand hier an diesem Ort eine prachtvolle Villa, und sie wurde von Graf Frederik und seiner von ihm ueber alles geliebten Frau Herliande bewohnt.]]
            },
            {
                Text = "Doch eines Tages wurde Herliande ploetzlich krank, sehr krank... sie starb keine Woche spaeter. Traurig, nicht wahr? Hi, hi! Nein, wirklich, ist schon traurig. Frederik konnte ueber den Schmerz nicht hinwegkommen, aus Gram fielen ihm alle seine Haare aus. Deswegen kennt man ihn heute als Frederik, den Kahlen. Bloeder Name fuer einen Mann so edler Herkunft, nicht? Hi,hi!"
            },
            {
                Text = [[
Naja, wie auch immer, Frederik liess seine Frau an ihrem gemeinsamen Lieblingsplatz begraben, und er schwor ihr, dass er, wenn seine Zeit gekommen ist, sich neben ihr begraben lassen wuerde.<BR>
<BR>
Hach, er haette nicht schwoeren sollen! Hi, hi!]]
            },
            {
                Text = "Denn es ging dann so weiter: eines Nachts, Jahre nach Herliande's Tod, brach ploetzlich ein Feuer in der Villa aus. Die Ursache konnte nie geklaert werden, aber das Haus brannte bis auf die Grundmauern nieder, und mit ihm auch Frederik. So sehr man auch nach seinen menschlichen Ueberresten suchte, nie hat sie wer gefunden. Tragisch. Hi, hi! Nein, im Ernst!"
            },
            {
                Text = "Nunja, du kannst dir ja sicher denken wie die Geschichte weitergeht: Frederik konnte keine Ruhe finden, weil er den Schwur, den er am Grab seiner Frau geleistet hatte, nicht einhalten konnte. Seither geistert er Nacht fuer Nacht durch diesen Park. Anscheinend hat er sich zuletzt bis auf die Burg gewagt. Schade dass ich unseren Weintraub nicht dabei gesehen habe! Hi, hi! Der hat sich sicher in die Hosen gemacht vor Angst!"
            },
            {
                Text = "Wenn ich nur die verflixten Gebeine Frederik's finden koennte! Man muesste sie dann nur im Grab seiner Frau orderntlich beisetzen, und der Fluch waere gebrochen! Dann haette auch Weintraub wieder seine Ruh fuer seinen ach so wichtigen Ball. Hi, hi! Mich haben sie ja nicht eingeladen, aber ich waere ja sowieso nicht hingegangen. Mir sind die dort alle zu aufgeblasen! Hi, hi, aufgeblasen!"
            },
            {
                Text = [[
Sag mal, warum hilfst du mir nicht einfach, die Gebeine zu suchen? Sie muessen hier irgendwo sein....<BR>
<BR>
&lt;Suche im Umkreis nach einem Micro mit den Gebeinen, notiere den Code und rede nochmal mit Ignatia&gt;]]
            }
        })
        ztaskFindeFrederiksGebeine.Active = true
    end
end
function cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB2(action)
    if action ~= nil then
    end
end
function cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB3(action)
    if action ~= nil then
    end
end
function cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB1(action)
    if action == "Button1" then
        Wherigo.MessageBox({
            Text = "\"Dann verliere er keine weitere Zeit! Gehe er zu Theo, unserem Gaertner! Der hat das Gespenst zuletzt gesehen.\". Ohne ein weiteres Wort zu verlieren verschwindet der Burgherr in seiner Burg, und ueberlaesst dich deinem Schicksal.",
            Callback = cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB3
        })
        ztaskGehezumBurgherrnundredemitihm.Complete = true
        if autosave == true then
            cartDasGeheimnisderverschwundenenVilla:RequestSync()
        end
        ztaskSprichmitTheo.Active = true
        zoneBurggarten.Visible = true
        zcharacterIgnatiaFeuerschwanz.Commands.Rede.Enabled = true
        zcharacterBurgherrvonWeintraub:MoveTo(nil)
    else
        Wherigo.MessageBox({
            Text = "\"Dann verschwinde er aus unseren Augen, der feige Hund!\"",
            Callback = cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB2
        })
    end
end
function cartDasGeheimnisderverschwundenenVilla.MsgBoxCBFuncs.MsgBoxCB4(action)
    if action ~= nil then
        Wherigo.ShowScreen(Wherigo.LOCATIONSCREEN)
        ztimerkraeutertimer:Start()
    end
end
