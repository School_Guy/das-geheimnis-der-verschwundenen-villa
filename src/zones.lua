function zoneBurgeingang:OnProximity()
    if zoneBurgeingang:Contains(zcharacterBurgherrvonWeintraub) and talkedBurgherr == true then
        Wherigo.MessageBox({
            Text = "Noch immer steht Burgherr von Weintraub vor dem Eingang seiner Burg. Vielleicht denkt er dass du deine Meinung noch aendern wirst?"
        })
    end
    if zoneBurgeingang:Contains(zcharacterBurgherrvonWeintraub) and talkedBurgherr == false then
        Wherigo.MessageBox({
            Text = "Du siehst schon von weitem Burgherr von Weintraub. Er scheint recht ungeduldig auf dich zu warten - und irgendwie wirkt er, als haette er vor etwas Angst..."
        })
    end
end

function zoneBurggarten:OnProximity()
    if ztaskSprichmitTheo.Complete == false and ztaskSprichmitTheo.Active == true then
        Wherigo.MessageBox({
            Text = "Du naeherst dich dem Burggarten. In einer Ecke siehst du einen Mann mit gruener Schuerze mit einer Schaufel den Boden bearbeiten - das wird wohl Theo, der Gaertner sein!"
        })
    end
end

function zoneBurgeingang:OnEnter()
    if zoneBurgeingang:Contains(zcharacterBurgherrvonWeintraub) then
        Wherigo.ShowScreen(Wherigo.ITEMSCREEN)
    end
end
function zoneBurggarten:OnEnter()
    Wherigo.ShowScreen(Wherigo.ITEMSCREEN)
    if gotkraut == true then
        zitemErnskraut.Commands.Gib.Enabled = true
    end
end
function zoneKraeuterwiese:OnEnter()
    if zoneKraeuterwiese:Contains(zitemErnskraut) then
        Wherigo.ShowScreen(Wherigo.ITEMSCREEN)
    end
end
function zoneVerschwundeneVilla:OnEnter()
    Wherigo.ShowScreen(Wherigo.ITEMSCREEN)
end

function zoneLuisenruhe:OnEnter()
    Wherigo.ShowScreen(Wherigo.DETAILSCREEN, zitemGrab)
end
function zoneVerschwundeneVilla:OnProximity()
    if talkedIgnatia ~= true then
        Wherigo.MessageBox({
            Text = [[
Du naeherst dich dem Ort, der die "verschwundene Villa" heisst. Tatsaechlich siehst du zwar etwas wie Eingangstreppen, aber kein Gebaeude zu dem sie fuehren.<BR>
<BR>
Auf der Wiese hinter den Treppen siehst du eine merkwuerdig gekleidete Frau, die offensichtlich etwas sucht.]]
        })
    end
end

function zoneLuisenruhe:OnProximity()
    if ztaskFindedasGrabvonFrederiksFrau.Active == true and visitluisenruhe == false then
        visitluisenruhe = true
        Wherigo.MessageBox({
            Text = "Das muss es sein - der Aussichtspunkt, der der Lieblingsplatz von Frederik und Herliande war! Und irgendwo in der Naehe muss das Grab sein..."
        })
        zitemGrab.Visible = true
    end
end
