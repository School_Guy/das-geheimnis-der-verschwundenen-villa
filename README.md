# Das Geheimnis der verschwundenen Villa

- Corresponding Geocache: [GC19N49](https://coord.info/GC19N49)
- Corresponding Cartridge (wherigo.com): none (currently)
- Corresponding Cartridge (Wherigo Foundation): none (currently)

Dieser Wherigo ist eine Reimplementierung der ursprünglichen Cartridge
von frigschnek. Diese soll sein Werk erhalten, obwohl die ursprünglichen
Quelldateien verloren gegangen sind.
